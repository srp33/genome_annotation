import os, sys

inVariantsFilePath = sys.argv[1]
inVariantImpactsFilePath = sys.argv[2]
inGeneDetailedFilePath = sys.argv[3]
outVariantInfoFilePath = sys.argv[4]
outGenicTranscriptsFilePath = sys.argv[5]
outNearbyGenesFilePath = sys.argv[6]

def writeTranscriptItems(lineItems):
    if lineItems == None:
        return

    transcript = lineItems[variantImpactsTranscriptIndex]

    if transcript not in geneDetailedDict:
        return

    outItems = [lineItems[i] for i in variantImpactsIndicesToKeep]
    outItems += [geneDetailedDict[transcript][i] for i in geneDetailedIndicesToKeep1]
    outGenicTranscriptsFile.write("\t".join(handleMissingValues(outItems)) + "\n")

def writeNearbyGeneItems(variantID, relativePosition, transcript):
    if transcript == None:
        return

    outItems = [variantID, relativePosition] + [geneDetailedDict[transcript][i] for i in geneDetailedIndicesToKeep2]
    outNearbyGenesFile.write("\t".join(handleMissingValues(outItems)) + "\n")

def handleMissingValues(x):
    for i in range(len(x)):
        if x[i] in missingOptions:
            x[i] = ""

    return x

missingOptions = set(["None"])

inVariantsFile = open(inVariantsFilePath)
inVariantImpactsFile = open(inVariantImpactsFilePath)
inGeneDetailedFile = open(inGeneDetailedFilePath)
outVariantInfoFile = open(outVariantInfoFilePath, 'w')
outGenicTranscriptsFile = open(outGenicTranscriptsFilePath, 'w')
outNearbyGenesFile = open(outNearbyGenesFilePath, 'w')

variantsHeaderItems = inVariantsFile.readline().rstrip().split("\t")
variantImpactsHeaderItems = inVariantImpactsFile.readline().rstrip().split("\t")
variantIndicesToKeep = [variantsHeaderItems.index(x) for x in variantsHeaderItems if x == "variant_id" or x not in ("anno_id", "info", "cosmic_ids", "somatic_score") or x not in variantImpactsHeaderItems]
variantImpactsIndicesToKeep = [variantImpactsHeaderItems.index(x) for x in variantImpactsHeaderItems if x not in ("anno_id", "polyphen_pred", "polyphen_score", "sift_pred", "sift_score")]
geneDetailedHeaderItems = inGeneDetailedFile.readline().rstrip().split("\t")
geneDetailedIndicesToKeep1 = [geneDetailedHeaderItems.index(x) for x in geneDetailedHeaderItems if x not in ("uid", "chrom", "gene", "transcript", "biotype")]
geneDetailedIndicesToKeep2 = [geneDetailedHeaderItems.index(x) for x in geneDetailedHeaderItems if x not in ("uid", "chrom")]

variantIDIndex = variantsHeaderItems.index("variant_id")
variantStartIndex = variantsHeaderItems.index("start")
variantEndIndex = variantsHeaderItems.index("end")

exonicIndex = variantImpactsHeaderItems.index("is_exonic")
codingIndex = variantImpactsHeaderItems.index("is_coding")
splicingIndex = variantImpactsHeaderItems.index("is_splicing")
lofIndex = variantImpactsHeaderItems.index("is_lof")
impactIndex = variantImpactsHeaderItems.index("impact")
codonChangeIndex = variantImpactsHeaderItems.index("codon_change")
variantImpactsTranscriptIndex = variantImpactsHeaderItems.index("transcript")
geneDetailedTranscriptIndex = geneDetailedHeaderItems.index("transcript")
geneDetailedGeneIndex = geneDetailedHeaderItems.index("gene")
geneDetailedStartIndex = geneDetailedHeaderItems.index("transcript_start")
geneDetailedEndIndex = geneDetailedHeaderItems.index("transcript_end")

geneDetailedDict = {}
geneStartEnd = []

for line in inGeneDetailedFile:
    lineItems = line.rstrip().split("\t")

    transcript = lineItems[geneDetailedTranscriptIndex]
    geneStart = int(lineItems[geneDetailedStartIndex])
    geneEnd = int(lineItems[geneDetailedEndIndex])

    geneDetailedDict[transcript] = lineItems
    geneStartEnd.append((transcript, geneStart, geneEnd))



outVariantInfoFile.write("\t".join([variantsHeaderItems[i] for i in variantIndicesToKeep]) + "\n")
outNearbyGenesFile.write("\t".join(["variant_id", "relative_position"] + [geneDetailedHeaderItems[i] for i in geneDetailedIndicesToKeep2]) + "\n")

variantCount = 0
for line in inVariantsFile:
    variantCount += 1
    if variantCount % 1000 == 0:
        print "%i variants processed" % variantCount
#########################################################
#    if variantCount > 10000:
#        break
#########################################################

    lineItems = line.rstrip().split("\t")

    variantID = lineItems[variantIDIndex]
    variantStart = int(lineItems[variantStartIndex])
    variantEnd = int(lineItems[variantEndIndex])

    outVariantInfoFile.write("\t".join(handleMissingValues([lineItems[i] for i in variantIndicesToKeep])) + "\n")

    upstreamGeneDict = {}
    downstreamGeneDict = {}
    for geneInfo in geneStartEnd:
        #if geneInfo[2] < variantStart and (variantStart - geneInfo[2]) < 1000000:
        if geneInfo[2] < variantStart:
            upstreamGeneDict[geneInfo[2]] = geneInfo[0]
        #if geneInfo[1] > variantEnd and (geneInfo[1] - variantEnd) < 1000000:
        if geneInfo[1] > variantEnd:
            downstreamGeneDict[geneInfo[1]] = geneInfo[0]

    closestUpstreamGene = None
    if len(upstreamGeneDict) > 0:
        closestUpstreamGene = upstreamGeneDict[max(upstreamGeneDict)]

    closestDownstreamGene = None
    if len(downstreamGeneDict) > 0:
        closestDownstreamGene = downstreamGeneDict[min(downstreamGeneDict)]

    writeNearbyGeneItems(variantID, "UPSTREAM", closestUpstreamGene)
    writeNearbyGeneItems(variantID, "DOWNSTREAM", closestDownstreamGene)




outGenicTranscriptsFile.write("\t".join([variantImpactsHeaderItems[i] for i in variantImpactsIndicesToKeep] + [geneDetailedHeaderItems[i] for i in geneDetailedIndicesToKeep1]) + "\n")

transcriptCount = 0
for line in inVariantImpactsFile:
    transcriptCount += 1
    if transcriptCount % 10000 == 0:
        print "%i transcripts processed" % transcriptCount
################################
#    if transcriptCount >= 50000:
#        break
################################

    transcriptItems = line.rstrip().split("\t")
    variantID = lineItems[0]

    if transcriptItems[exonicIndex] == "1" or transcriptItems[codingIndex] == "1" or transcriptItems[splicingIndex] == "1" or transcriptItems[lofIndex] == "1":
        writeTranscriptItems(transcriptItems)

outVariantInfoFile.close()
outNearbyGenesFile.close()
outGenicTranscriptsFile.close()
inGeneDetailedFile.close()
inVariantImpactsFile.close()
inVariantsFile.close()
